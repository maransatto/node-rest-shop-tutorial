const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mysql = require('mysql');

const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');
const userMysqlRoutes = require('./api/routes/users-mysql');
const userRoutes = require('./api/routes/users');

// conecta com o mongodb
mongoose.connect('mongodb://' + process.env.MONGO_ATLAS_PW + ':teste@cluster0-shard-00-00-9yjem.mongodb.net:27017,cluster0-shard-00-01-9yjem.mongodb.net:27017,cluster0-shard-00-02-9yjem.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true');
mongoose.Promise = global.Promise;

// Cria conexão com o banco de dados
app.use((req, res, next) => {
    res.locals.connection = mysql.createConnection({
        host        : process.env.MYSQL_HOST,
        user        : process.env.MYSQL_USER,
        password    : process.env.MYSQL_PASSWORD,
        database    : process.env.MYSQL_DATABASE
    });
    res.locals.connection.connect();
    console.log("Conexão aberta com o Mysql...");
    next();
});


app.use(morgan('dev'));
app.use('/uploads', express.static('uploads')); // Deixa a pasta uploads acessível para as imagens
app.use(bodyParser.urlencoded({extended: false})); // informa que aceita urlencoded
app.use(bodyParser.json()); // informa que aceita json como parâmetro

// Inclusão de Headers para controle de acessos
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'); // allow access from anywhere (if it were specific, it should be something like: http://my-cool-page.com instead of *)
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Conten-Type, Accept, Autorazation'
    ); // define quais os headers permitidos

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET'); // Quais os métodos possíveis
        return res.status(200).json({});
    }
    next();
});

// Routes which should handle requests
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/users-mysql', userMysqlRoutes);
app.use('/user', userRoutes);

// Se passar pelos dois middleweres acima, quer dizer que procura uma página que não existe
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

// Trata qualquer tipo de erro na aplicação (ex: quando erro vem do banco de dados, cai direto aqui)
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;