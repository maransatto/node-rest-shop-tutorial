// Busca referência do http
const http = require("http");
const app = require('./app');

// Busca a porta da variável de ambiente ou default 3000
const port = process.env.PORT || 3000;

// Cria o server
const server = http.createServer(app);

// "Escuta" o server na porta definida anteriormente
server.listen(port);