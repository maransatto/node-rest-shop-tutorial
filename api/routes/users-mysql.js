const express = require('express');
const router = express.Router();


router.get('/', (req, res, next) => {
    res.locals.connection.query('SELECT * FROM users', (error, results, fields) => {
        if (error) {
            res.send(JSON.stringify({
                "status" : 500,
                "error" : error,
                "response" : null
            }));
        } else {
            res.send(JSON.stringify({
                "status" : 200,
                "error" : null,
                "response" : results
            }));
        }
    });
});


module.exports = router;