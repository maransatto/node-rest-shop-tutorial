# Como criar RESTful API com Node.js e MongoDB

Este projeto foi criado seguindo os passos dessa sequencia de estudos encontrada no youtube que deu muito certo pra mim. Ele explica muito bem o conceito e explica de forma bem simples o "como":

- [**1. O que é uma RESTful API?**](https://www.youtube.com/watch?v=0oXYLzuucwE)
- [**2. Planejando & Primeiros Passos**](https://www.youtube.com/watch?v=blQ60skPzl0)
- [**3. Adicionando mais Rotas na API**](https://www.youtube.com/watch?v=FV1Ugv1Temg)
- [**4. Tratando erros & Melhorando a configuração do Projeto**](https://www.youtube.com/watch?v=UVAMha41dwo)
- [**5. Analisando o Corpo & Tratando CORS**](www.youtube.com/watch?v=zoSJ3bNGPp0)
- [**6. MongoDB e Mongoose**](https://youtu.be/WDrU305J1yw)
- [**7. Validação com Mongoose**](https://www.youtube.com/watch?v=CMDsTMV2AgI)
- [**8. Fazendo upload de imagem**](https://www.youtube.com/watch?v=srPXMt1Q0nY)
- [**9. Adicionando Cadastro de Usuário**](https://www.youtube.com/watch?v=_EP2qCmLzSE)
- [**10. Adicionando Login de Usuário**](https://www.youtube.com/watch?v=0D5EEKH97NA)
- [**11. Proteção de Rota JWT**](https://www.youtube.com/watch?v=8Ip0pcwbWYM)
- [**12. Adicionando Controllers**](https://www.youtube.com/watch?v=ucuNgSOFDZ0)


## Passo a Passo

> Num futuro não tão distante estarei incluindo aqui um passo a passo por escrito em português

### 1. Criando o projeto

- ```npm init```

### 2. Instalando o Express

- ```npm install --save express```


### X. Iniciando o serviço

- ```npm start```

## Instalação dos pacotes

### Express

> Este pacote (pelo que entendi até o momento) é a biblioteca necessária para criar o serviço de HTTP.

- ```npm install --save express```

### Nodemon

> Este pacote permite que o serviço seja reiniciado automaticamente após salvar alguma alteração em desenvolvimento

- ```npm install --save-dev nodemon```

### Morgan

> Utilizado para registrar das chamadas da API. (*não é o Morgan da série "The Walking Dead"*)

- ```npm install --save-dev morgan```

### Body-Parser

> Usado para analisar e extrar o corpo da requisição (request bodies)

- ```npm install --save body-parser```

### Mongoose

> Responsável por conectar com o mongodb

- ```npm install --save mongoose```

### Multer

> Aceita outra alternativa de body-parser para aceitar arquivos

- ```npm install --save multer```

### node.bcrypt.js

> Para criptografar a senha do usuário

-  ```npm install --save bcrypt```

### JSON Web Token

> Pacote que permite criar token de autenticação de usuário

- ```npm install --save jsonwebtoken```

------------

# Com Mysql

## Instalação dos pacotes

### Mysql

> Tá bem intuitivo esse.

- ```npm install --save mysql```



Agora seguindo os passos deste aqui:

https://medium.com/@avanthikameenakshi/building-restful-api-with-nodejs-and-mysql-in-10-min-ff740043d4be